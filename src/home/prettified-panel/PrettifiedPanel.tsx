import { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { HiClipboardCopy } from "react-icons/hi";
import { Tooltip as ReactTooltip } from "react-tooltip";
import JSONPretty from "react-json-pretty";
import "react-tooltip/dist/react-tooltip.css";
import "./PrettifiedPanel.css";

type AppProps = {
    prettyText: string, 
    showPrettifiedText: boolean
}

const PrettifiedPanel = ({ prettyText, showPrettifiedText }: AppProps) => {

    const JSONPrettyMon = require("react-json-pretty/dist/monikai");
    const [showCopyTooltip, setShowCopyTooltip] = useState(false);

    const onClickCopy = () => {
        setShowCopyTooltip(true);
    }

    const onCopyMouseLeave = () => {
        setShowCopyTooltip(false);
    }

    const renderPrettifiedText = () => {
        if(showPrettifiedText) {
            return <div className="pretty-text">
                    <div className="pretty-text-options">
                        <CopyToClipboard text={prettyText}
                            onCopy={onClickCopy}>
                            <button className="copy-button" onMouseLeave={onCopyMouseLeave} id="is-copied" data-tooltip-content="copied!">
                                <HiClipboardCopy size={20}/>
                            </button>
                        </CopyToClipboard>
                    </div>

                    <JSONPretty data={prettyText} theme={JSONPrettyMon}
                        style={{
                            textAlign: "left",
                            fontFamily: "monospace"
                        }} 
                    />

                    <ReactTooltip className="tooltip" anchorId="is-copied" isOpen={showCopyTooltip}/>
            </div>
        } else {
            return <></>;
        }
        
    }

    return <div>
        {renderPrettifiedText()}
    </div>;

}

export default PrettifiedPanel;