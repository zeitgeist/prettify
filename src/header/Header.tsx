import "./Header.css";

const Header = () => {

    return <div className="header-wrapper">
        <a href="/"><span>{"{}"}</span> Prettify </a>        
    </div>

}

export default Header;