import "./Home.css";
import { useState } from 'react';
import PrettifiedPanel from "./prettified-panel/PrettifiedPanel";

const Home = () => {

    const [text, setText] = useState("");
    const [showPrettifiedText, setShowPrettifiedText] = useState(false);
    const [prettyText, setPrettyText] = useState("");

    const processText = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if(text.length > 0) {
            setShowPrettifiedText(true);
            const parsedJson = JSON.parse(text);
            setPrettyText(JSON.stringify(parsedJson, null, 2));
        }else {
            setShowPrettifiedText(false);
        }

    }

    const onTextChange = (event: React.FormEvent<HTMLTextAreaElement>):void => {
        setShowPrettifiedText(false);
        setText(event.currentTarget.value);
    }

    return <div className="home-wrapper">
        <form className="form" onSubmit={processText}>
            <textarea value={text} rows={20} cols={70} placeholder="Add your ugly JSON here." onChange={onTextChange} required/>
            <div className="submit-button-wrapper">
                <input type="submit" value="Prettify"/>
            </div>
        </form>        

        <PrettifiedPanel showPrettifiedText={showPrettifiedText} prettyText={prettyText}/>
    </div>

}

export default Home;